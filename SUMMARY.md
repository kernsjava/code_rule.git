# Summary

* [简介](README.md)
* [编程规范](/chapter1/README.md)
   * [命名风格](/chapter1/section1.md)
   * [常量定义](/chapter1/section2.md)
   * [代码格式](/chapter1/section3.md)
   * [OOP规约](/chapter1/section4.md)
   * [集合操作](/chapter1/section5.md)
   * [并发操作](/chapter1/section6.md)
   * [控制语句](/chapter1/section7.md)
   * [注释规范](/chapter1/section8.md)
   * [前后端交互](/chapter1/section9.md)
   * [其它](/chapter1/section10.md)
* [通用字段](/chapter2/README.md)  
* [日志](/chapter3/README.md)  
   * [异常](/chapter3/section1.md)
   * [规约](/chapter3/section1.md)


